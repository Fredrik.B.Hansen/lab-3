package no.uib.inf101.terminal;

public class CmdEcho implements Command  {

    @Override
    public String run(String[] args){
        String ord = "";
        for(String i : args){
            ord += i+" ";  
        }
        return ord;
    }
    


    @Override
    public String getName(){
        return "echo";
    }

}
